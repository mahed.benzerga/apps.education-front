import { 
  applications,
  applicationsGeneral, 
  bonnesPratiques, 
  platform, 
  cgu, 
  accessibilite, 
  legal, 
  donneesPersonnelles,
  aPropos
} from '../data';

export default {
  findApplicationById(applicationId) {
    return new Promise((resolve) => {
      return resolve(applications.find((application) => application.id === applicationId));
    })
  },
  findAllApplications() {
    return new Promise((resolve) => {
      resolve(applications)
    });
  },
  findApplicationGeneralById(applicationId) {
    return new Promise((resolve) => {
      return resolve(applicationsGeneral.find((application) => application.id === applicationId));
    })
  },
  findAllApplicationsGeneral() {
    return new Promise((resolve) => {
      resolve(applicationsGeneral)
    });
  },
  findAcademieById(academieId) {
    return new Promise((resolve) => {
      return resolve(platform.academies.find((academie) => academie.id === academieId));
    })
  },
  findAllAcademies() {
    return new Promise((resolve) => {
      return resolve(platform.academies)
    })
  },
  bonnesPratiques() {
    return new Promise((resolve) => {
      return resolve(bonnesPratiques)
    })
  },
  cgu() {
    return new Promise((resolve) => {
      return resolve(cgu);
    })
  },
  accessibilite() {
    return new Promise((resolve) => {
      return resolve(accessibilite);
    })
  },
  legal() {
    return new Promise((resolve) => {
      return resolve(legal);
    })
  },
  donneesPersonnelles() {
    return new Promise((resolve) => {
      return resolve(donneesPersonnelles);
    })
  },
  aPropos() {
    return new Promise((resolve) => {
      return resolve(aPropos);
    })
  }
}