import Vue from "vue";

export default Vue.observable({
  isPlatformAccessShown: false
});