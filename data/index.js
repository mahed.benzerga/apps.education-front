
export {default as applications} from './applications';
export {default as applicationsGeneral} from './applicationsGeneral';
export {default as platform} from './platform';
export {default as bonnesPratiques} from './bonnesPratiques';
export {default as cgu} from './cgu';
export {default as accessibilite} from './accessibilite';
export {default as legal} from './legal';
export {default as donneesPersonnelles} from './donneesPersonnelles';
export {default as aPropos} from './aPropos';