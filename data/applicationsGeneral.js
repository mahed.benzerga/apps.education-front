export default [
  {
    "id": "visio-cned",
    "title": "Visio CNED",
    "url": "https://classesvirtuelles.cned.fr/",
    "category": "webconference",
    "subtitle": "Mes classes virtuelles CNED",
    "icon": "webconference",
  },
  {
    "id": "visio-agents",
    "title": "Webinaire",
    "url": "https://visio-agents.education.fr/",
    "category": "webconference",
    "subtitle": "Réunions visio entre agents",
    "icon": "webconference",
  },
  {
    "id": "rendez-vous",
    "title": "Rendez-vous",
    "url": "https://rendez-vous.renater.fr/",
    "category": "webconference",
    "subtitle": "Réunions",
    "icon": "webconference",
  },
  {
    "id": "magistere",
    "title": "Magistère",
    "url": "https://magistere.education.fr/",
    "category": "general",
    "subtitle": "Formation continue",
    "icon": "etherpad",
  },
  {
    "id": "evento",
    "title": "Evento",
    "url": "https://evento.renater.fr/",
    "category": "general",
    "subtitle": "Planification d'événements",
    "icon": "blogs",
  },
  {
    "id": "tribu",
    "title": "Tribu",
    "url": "https://tribu.phm.education.gouv.fr/",
    "category": "general",
    "subtitle": "Espaces collaboratifs",
    "icon": "blogs",
  },
  {
    "id": "filesender",
    "title": "Filesender",
    "url": "https://filesender.renater.fr/",
    "category": "general",
    "subtitle": "Transfert de fichiers volumineux",
    "icon": "nextcloud",
  },
  {
    "id": "tchap",
    "title": "Tchap",
    "url": "https://www.tchap.gouv.fr/",
    "category": "general",
    "subtitle": "Messagerie de l'État",
    "icon": "forum",
  },
  {
    "id": "mastodon",
    "title": "Mastodon",
    "url": "https://mastodon.etalab.gouv.fr/",
    "category": "general",
    "subtitle": "Réseau social décentralisé",
    "icon": "blogs",
  },

];