export default `
<h1>Mentions légales</h1>

<h2>Sommaire</h2>
  <ul>
	<li><a href="#apps">Mentions légales - Site apps.education.fr et plateforme de présentation de services</a></li>
	<li><a href="#sso">Mentions légales - Service de SSO</a></li>
	<li><a href="#forums">Mentions légales - Forums</a></li>
	<li><a href="#cloud">Mentions légales - Nextcloud</a></li>
	<li><a href="#pads">Mentions légales - Pads</a></li>
  	<li><a href="#peertube">Mentions légales - Peertube</a></li>
  	<li><a href="#codiMD">Mentions légales - CodiMD</a></li>
  </ul>



<h2 id="apps"><strong>Mentions légales - Site apps.education.fr et plateforme de présentation de services</strong></h2>

<h3>Présentation</h3>
	<p>Ce site web apps.education.fr ainsi que la plateforme de présentation des services apps-(nom de mon académie).beta.education.fr sont proposés jusqu'à la fin de l'année scolaire 2019-2020. Ils proposent un ensemble de services de communication et de partage de documents transmedia réservés aux personnels de l'éducation nationale.</p>

	<h3>Identification de l'éditeur</h3>
		<address>
			Ministère de l'Éducation nationale et de la jeunesse
			Direction du numérique pour l'éducation 
			110 rue Grenelle, 75007 Paris
		</address>


	<h3>Directeur de publication</h3>
		<p>M. Jean-Marc Merriaux, 
		Directeur du numérique pour l’éducation</p>

	<h3>Hébergement : </h3>
		<p>
			<ul>
				<li>Le site apps.education.fr est hébergé par le ministère de l'Éducation nationale sur les infrastructures mutualisées PHM à Osny (95).</li>
				<li>La plateforme de présentation des services applicatifs est hébergée par OVH dans le centre de données de Strasbourg, 9 Rue du Bassin de l'Industrie, 67000 Strasbourg</li>
			</ul>
<p>Ces services constituent un traitement de données à caractère personnel mis en œuvre par le ministre de l’éducation nationale et de la jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).</p>
<p>Le ministère de l’éducation nationale et de la jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.</p>
<p>Les indications précises relatives au traitement des données à caractère personnel sont disponible dans le document <a href="https://apps.education.fr/donnees-personnelles">Protection et traitement des données à caractère personnel</a>.</p>



<h2 id="sso"><strong>Mentions légales - Service de SSO</strong></h2>


<h3>Présentation</h3>
	<p>Ce service sso-(nom de mon académie).beta.education.fr est proposé jusqu'à la fin de l'année scolaire 2019-2020. Il permet à l’utilisateur identifié d'accéder à un ensemble de services de communication et de partage de documents transmedia réservés en écriture aux personnels de l'éducation nationale.</p>

	<h3>Identification de l'éditeur</h3>
		<address>
			Ministère de l'Éducation nationale et de la jeunesse
			Direction du numérique pour l'éducation 
			110 rue Grenelle, 75007 Paris
		</address>


	<h3>Directeur de publication</h3>
		<p>M. Jean-Marc Merriaux, 
		Directeur du numérique pour l’éducation</p>

	<h3>Maîtrise d'ouvrage et rédaction</h3>
		<p>La sous direction du Socle assure la coordination du processus de production, de validation et d'accessibilité des services conçus par la direction du numérique pour l’éducation, les académies ou les institutions et organismes partenaires, ainsi que le développement des applications, l'administration du site et des services et l'accompagnement exécutif de la gouvernance du projet dans le cadre du projet “Services numériques partagés” (SNP).</p>
		<p>
			<ul>
				<li>Responsable de production : Laurent Le Prieur </li>
				<li>Chef de projet : Marine Gout</li>
				<li>Coordination des développements : Nicolas Schont</li>
			</ul>
		</p>

	<h3>Accompagnement et support</h3>
		<p>Assistance de niveau 1 aux utilisateurs : DANE de l’académie de l’enseignant</p>
		<p>Assistance technique de niveau 2 et maintenance : Pôle de compétences Logiciels Libres, DSI Académie de Dijon, 2 G rue du général Delaborde, 21000 Dijon</p>
		<p>Courriel: eole@ac-dijon.fr</p>
		<p>Par messagerie instantanée: irc.eu.freenode.net canal #Eol</p>



	<h3>Hébergement : </h3>
		<p>Le service de SSO est hébergé par OVH dans le centre de données de Strasbourg, 9 Rue du Bassin de l'Industrie, 67000 Strasbourg</p>
		
		<p>Ce service de SSO constitue un traitement de données à caractère personnel mis en œuvre par le ministre de l’éducation nationale et de la jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).</p>
		<p>Le ministère de l’éducation nationale et de la jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.</p>
		<p>Les indications précises relatives au traitement des données à caractère personnel sont disponible dans le document <a href="https://apps.education.fr/donnees-personnelles">Protection et traitement des données à caractère personnel</a>.</p>


<h2 id="forums"><strong>Mentions légales - Forums</strong></h2>


	<h3>Présentation</h3>
		<p>Ce service forum-(nom de l’académie).beta.education.fr est proposé jusqu'à la fin de l'année scolaire 2019-2020. Il permet à l’utilisateur identifié d'accéder à un service de forum permettant la communication et le partage de documents transmedia. Le forum est réservés en écriture aux personnels de l'éducation nationale. L’accès en lecture au service de forum est public.</p>

	<h3>Identification de l'éditeur</h3>
		<address>
			Ministère de l'Éducation nationale et de la jeunesse
			Direction du numérique pour l'éducation 
			110 rue Grenelle, 75007 Paris
		</address>


	<h3>Directeur de publication</h3>
		<p>M. Jean-Marc Merriaux, 
		Directeur du numérique pour l’éducation</p>

	<h3>Maîtrise d'ouvrage et rédaction</h3>
		<p>La sous direction du Socle assure la coordination du processus de production, de validation et d'accessibilité des services conçus par la direction du numérique pour l’éducation, les académies ou les institutions et organismes partenaires, ainsi que le développement des applications, l'administration du site et des services et l'accompagnement exécutif de la gouvernance du projet dans le cadre du projet “Services numériques partagés” (SNP).</p>
		<p>
			<ul>
				<li>Responsable de production : Laurent Le Prieur </li>
				<li>Chef de projet : Marine Gout</li>
				<li>Coordination des développements : Nicolas Schont</li>
			</ul>
		</p>

	<h3>Accompagnement et support</h3>
		<p>Assistance de niveau 1 aux utilisateurs : DANE de l’académie de l’enseignant</p>
		<p>Assistance technique de niveau 2 et maintenance : Pôle de compétences Logiciels Libres, DSI Académie de Dijon, 2 G rue du général Delaborde, 21000 Dijon</p>
		<p>Courriel: eole@ac-dijon.fr</p>
		<p>Par messagerie instantanée: irc.eu.freenode.net canal #Eol</p>



	<h3>Hébergement : </h3>
		<p>Le service de Forums est hébergé par OVH dans le centre de données de Strasbourg, 9 Rue du Bassin de l'Industrie, 67000 Strasbourg</p>
		
		<p>Ce service de Forums constitue un traitement de données à caractère personnel mis en œuvre par le ministre de l’éducation nationale et de la jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).</p>
		<p>Le ministère de l’éducation nationale et de la jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.</p>
		<p>Les indications précises relatives au traitement des données à caractère personnel sont disponible dans le document <a href="https://apps.education.fr/donnees-personnelles">Protection et traitement des données à caractère personnel</a>.</p>


<h2 id="cloud"><strong>Mentions légales - Nextcloud</strong></h2>


	<h3>Présentation</h3>
		<p>Ce service de publication de fichiers accessible à l’adresse nuage-(nom de l’académie).beta.education.fr est proposé jusqu'à la fin de l'année scolaire 2019-2020. Il permet à l’utilisateur identifié d'accéder à un service de stockage et de publication de fichiers permettant la communication et le partage de documents transmedia. Le service de stockage et de publication de fichiers est accessible en écriture aux personnels de l'éducation nationale. Les élèves peuvent être invités à déposer ou à modifier des documents. L’accès en lecture au service de publication de fichiers peut être public.</p>

	<h3>Identification de l'éditeur</h3>
		<address>
			Ministère de l'Éducation nationale et de la jeunesse
			Direction du numérique pour l'éducation 
			110 rue Grenelle, 75007 Paris
		</address>


	<h3>Directeur de publication</h3>
		<p>M. Jean-Marc Merriaux, 
		Directeur du numérique pour l’éducation</p>

	<h3>Maîtrise d'ouvrage et rédaction</h3>
		<p>La sous direction du Socle assure la coordination du processus de production, de validation et d'accessibilité des services conçus par la direction du numérique pour l’éducation, les académies ou les institutions et organismes partenaires, ainsi que le développement des applications, l'administration du site et des services et l'accompagnement exécutif de la gouvernance du projet dans le cadre du projet “Services numériques partagés” (SNP).</p>
		<p>
			<ul>
				<li>Responsable de production : Laurent Le Prieur </li>
				<li>Chef de projet : Marine Gout</li>
				<li>Coordination des développements : Nicolas Schont</li>
			</ul>
		</p>

	<h3>Accompagnement et support</h3>
		<p>Assistance de niveau 1 aux utilisateurs : DANE de l’académie de l’enseignant</p>
		<p>Assistance technique de niveau 2 et maintenance : Pôle de compétences Logiciels Libres, DSI Académie de Dijon, 2 G rue du général Delaborde, 21000 Dijon</p>
		<p>Courriel: eole@ac-dijon.fr</p>
		<p>Par messagerie instantanée: irc.eu.freenode.net canal #Eol</p>



	<h3>Hébergement : </h3>
		<p>Le service de Forums est hébergé par OVH dans le centre de données de Strasbourg, 9 Rue du Bassin de l'Industrie, 67000 Strasbourg</p>
		
		<p>Ce service d’hébergement de fichiers constitue un traitement de données à caractère personnel mis en œuvre par le ministre de l’éducation nationale et de la jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).</p>
		<p>Le ministère de l’éducation nationale et de la jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.</p>
		<p>Les indications précises relatives au traitement des données à caractère personnel sont disponible dans le document <a href="https://apps.education.fr/donnees-personnelles">Protection et traitement des données à caractère personnel</a>.</p>



<h2 id="pads"><strong>Mentions légales - Pads</strong></h2>


	<h3>Présentation</h3>
		<p>Ce service de création et d’édition de textes accessible à l’adresse pad-(nom de l’académie).beta.education.fr est proposé jusqu'à la fin de l'année scolaire 2019-2020. Il permet à l’utilisateur identifié d'accéder à un éditeur de texte et à un stockage de textes permettant la communication et le partage de documents transmedia. Le service d’édition de texte est accessible en écriture aux personnels de l'éducation nationale. Les élèves peuvent être invités à à modifier des textes sous la responsabilité des enseignants. L’accès en lecture au service de publication de fichiers peut être public.</p>

	<h3>Identification de l'éditeur</h3>
		<address>
			Ministère de l'Éducation nationale et de la jeunesse
			Direction du numérique pour l'éducation 
			110 rue Grenelle, 75007 Paris
		</address>


	<h3>Directeur de publication</h3>
		<p>M. Jean-Marc Merriaux, 
		Directeur du numérique pour l’éducation</p>

	<h3>Maîtrise d'ouvrage et rédaction</h3>
		<p>La sous direction du Socle assure la coordination du processus de production, de validation et d'accessibilité des services conçus par la direction du numérique pour l’éducation, les académies ou les institutions et organismes partenaires, ainsi que le développement des applications, l'administration du site et des services et l'accompagnement exécutif de la gouvernance du projet dans le cadre du projet “Services numériques partagés” (SNP).</p>
		<p>
			<ul>
				<li>Responsable de production : Laurent Le Prieur </li>
				<li>Chef de projet : Marine Gout</li>
				<li>Coordination des développements : Nicolas Schont</li>
			</ul>
		</p>

	<h3>Accompagnement et support</h3>
		<p>Assistance de niveau 1 aux utilisateurs : DANE de l’académie de l’enseignant</p>
		<p>Assistance technique de niveau 2 et maintenance : Pôle de compétences Logiciels Libres, DSI Académie de Dijon, 2 G rue du général Delaborde, 21000 Dijon</p>
		<p>Courriel: eole@ac-dijon.fr</p>
		<p>Par messagerie instantanée: irc.eu.freenode.net canal #Eol</p>



	<h3>Hébergement : </h3>
		<p>Le service de Pads est hébergé par OVH dans le centre de données de Strasbourg, 9 Rue du Bassin de l'Industrie, 67000 Strasbourg</p>
		
		<p>Ce service d’hébergement de fichiers constitue un traitement de données à caractère personnel mis en œuvre par le ministre de l’éducation nationale et de la jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).</p>
		<p>Le ministère de l’éducation nationale et de la jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.</p>
		<p>Les indications précises relatives au traitement des données à caractère personnel sont disponible dans le document <a href="https://apps.education.fr/donnees-personnelles">Protection et traitement des données à caractère personnel</a>.</p>


<h2 id="peertube"><strong>Mentions légales - Peertube</strong></h2>


	<h3>Présentation</h3>
		<p>Ce service de publication de vidéogrammes accessible à l’adresse tube(nom de l’académie).beta.education.fr est proposé jusqu'à la fin de l'année scolaire 2019-2020. Il permet à l’utilisateur identifié d'accéder à service de stockage et de diffusion de vidéogrammes. Le service de publication de vidéogrammes est accessible en écriture aux personnels de l'éducation nationale. L’accès en lecture au service de publication de fichiers peut être public.</p>

	<h3>Identification de l'éditeur</h3>
		<address>
			Ministère de l'Éducation nationale et de la jeunesse
			Direction du numérique pour l'éducation 
			110 rue Grenelle, 75007 Paris
		</address>


	<h3>Directeur de publication</h3>
		<p>M. Jean-Marc Merriaux, 
		Directeur du numérique pour l’éducation</p>

	<h3>Maîtrise d'ouvrage et rédaction</h3>
		<p>La sous direction du Socle assure la coordination du processus de production, de validation et d'accessibilité des services conçus par la direction du numérique pour l’éducation, les académies ou les institutions et organismes partenaires, ainsi que le développement des applications, l'administration du site et des services et l'accompagnement exécutif de la gouvernance du projet dans le cadre du projet “Services numériques partagés” (SNP).</p>
		<p>
			<ul>
				<li>Responsable de production : Laurent Le Prieur </li>
				<li>Chef de projet : Marine Gout</li>
				<li>Coordination des développements : Nicolas Schont</li>
			</ul>
		</p>

	<h3>Accompagnement et support</h3>
		<p>Assistance de niveau 1 aux utilisateurs : DANE de l’académie de l’enseignant</p>
		<p>Assistance technique de niveau 2 et maintenance : Pôle de compétences Logiciels Libres, DSI Académie de Dijon, 2 G rue du général Delaborde, 21000 Dijon</p>
		<p>Courriel: eole@ac-dijon.fr</p>
		<p>Par messagerie instantanée: irc.eu.freenode.net canal #Eol</p>



	<h3>Hébergement : </h3>
		<p>Le service Peertube est hébergé par Scaleway à Paris, ONLINE SAS, BP 438, 75366 PARIS CEDEX 08</p>
		
		<p>Ce service de publication de vidéogrammes constitue un traitement de données à caractère personnel mis en œuvre par le ministre de l’éducation nationale et de la jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).</p>
		<p>Le ministère de l’éducation nationale et de la jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.</p>
		<p>Les indications précises relatives au traitement des données à caractère personnel sont disponible dans le document <a href="https://apps.education.fr/donnees-personnelles">Protection et traitement des données à caractère personnel</a>.</p>

<h2 id="CodiMD"><strong>Mentions légales - CodiMD</strong></h2>


	<h3>Présentation</h3>
		<p>Ce service de création et d’édition de textes accessible à l’adresse codi-(nom de l’académie).beta.education.fr est proposé jusqu'à la fin de l'année scolaire 2019-2020. Il permet à l’utilisateur identifié d'accéder à un éditeur de texte et à un stockage de textes permettant la communication et le partage de documents transmedia. Le service d’édition de texte est accessible en écriture aux personnels de l'éducation nationale. Les élèves peuvent être invités à à modifier des textes sous la responsabilité des enseignants. L’accès en lecture au service de publication de fichiers peut être public.</p>

	<h3>Identification de l'éditeur</h3>
		<address>
			Ministère de l'Éducation nationale et de la jeunesse
			Direction du numérique pour l'éducation 
			110 rue Grenelle, 75007 Paris
		</address>


	<h3>Directeur de publication</h3>
		<p>M. Jean-Marc Merriaux, 
		Directeur du numérique pour l’éducation</p>

	<h3>Maîtrise d'ouvrage et rédaction</h3>
		<p>La sous direction du Socle assure la coordination du processus de production, de validation et d'accessibilité des services conçus par la direction du numérique pour l’éducation, les académies ou les institutions et organismes partenaires, ainsi que le développement des applications, l'administration du site et des services et l'accompagnement exécutif de la gouvernance du projet dans le cadre du projet “Services numériques partagés” (SNP).</p>
		<p>
			<ul>
				<li>Responsable de production : Laurent Le Prieur </li>
				<li>Chef de projet : Marine Gout</li>
				<li>Coordination des développements : Nicolas Schont</li>
			</ul>
		</p>

	<h3>Accompagnement et support</h3>
		<p>Assistance de niveau 1 aux utilisateurs : DANE de l’académie de l’enseignant</p>
		<p>Assistance technique de niveau 2 et maintenance : Pôle de compétences Logiciels Libres, DSI Académie de Dijon, 2 G rue du général Delaborde, 21000 Dijon</p>
		<p>Courriel: eole@ac-dijon.fr</p>
		<p>Par messagerie instantanée: irc.eu.freenode.net canal #Eol</p>



	<h3>Hébergement : </h3>
		<p>Le service CodiMD est hébergé par OVH dans le centre de données de Strasbourg, 9 Rue du Bassin de l'Industrie, 67000 Strasbourg</p>
		
		<p>Ce service d’hébergement de fichiers constitue un traitement de données à caractère personnel mis en œuvre par le ministre de l’éducation nationale et de la jeunesse (110 Rue de Grenelle 75007 Paris) pour l’exécution d’une mission d’intérêt public au sens du e) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).</p>
		<p>Le ministère de l’éducation nationale et de la jeunesse s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés et du RGPD.</p>
		<p>Les indications précises relatives au traitement des données à caractère personnel sont disponible dans le document <a href="https://apps.education.fr/donnees-personnelles">Protection et traitement des données à caractère personnel</a>.</p>



`