export default {
  "academies": [
      {
        "id": "aix-marseille",
        "name": "Académie d'Aix-Marseille",
        "url": "https://apps-Aix-Marseille.beta.education.fr"
      },
      {
        "id": "amiens",
        "name": "Académie d'Amiens",
        "url": "https://apps-Amiens.beta.education.fr"
      },
      {
        "id": "besancon",
        "name": "Académie de Besançon",
        "url": "https://apps-Besancon.beta.education.fr"
      },
      {
        "id": "bordeaux",
        "name": "Académie de Bordeaux",
        "url": "https://apps-Bordeaux.beta.education.fr"
      },
      {
        "id": "clermont-ferrand",
        "name": "Académie de Clermont-Ferrand",
        "url": "https://apps-Clermont-Ferrand.beta.education.fr"
      },
      {
        "id": "corse",
        "name": "Académie de Corse",
        "url": "https://apps-Corse.beta.education.fr"
      },
      {
        "id": "creteil",
        "name": "Académie de Créteil",
        "url": "https://apps-Creteil.beta.education.fr"
      },
      {
        "id": "dijon",
        "name": "Académie de Dijon",
        "url": "https://apps-Dijon.beta.education.fr"
      },
      {
        "id": "grenoble",
        "name": "Académie de Grenoble",
        "url": "https://apps-Grenoble.beta.education.fr"
      },
      {
        "id": "guadeloupe",
        "name":"Académie de Guadeloupe",
        "url":"https://apps-outremer.beta.education.fr"
      },
      {
        "id": "guyane",
        "name":"Académie de Guyane",
        "url":"https://apps-outremer.beta.education.fr"
      },
      {
        "id": "outre-mer",
        "name": "Académies d'Outre Mer",
        "url": "https://apps-outremer.beta.education.fr"
      },
      {
        "id": "lille",
        "name": "Académie de Lille",
        "url": "https://apps-Lille.beta.education.fr"
      },
      {
        "id": "limoges",
        "name": "Académie de Limoges",
        "url": "https://apps-Limoges.beta.education.fr"
      },
      {
        "id": "lyon",
        "name": "Académie de Lyon",
        "url": "https://apps-Lyon.beta.education.fr"
      },
      {
        "id": "mayotte",
        "name":"Académie de Mayotte",
        "url":"https://apps-outremer.beta.education.fr"
      },
      {
        "id": "martinique",
        "name":"Académie de Martinique",
        "url":"https://apps-outremer.beta.education.fr"
      },
      {
        "id": "montpellier",
        "name": "Académie de Montpellier",
        "url": "https://apps-Montpellier.beta.education.fr"
      },
      {
        "id": "nancy",
        "name": "Académie de Nancy",
        "url": "https://apps-Nancy.beta.education.fr"
      },
      {
        "id": "nantes",
        "name": "Académie de Nantes",
        "url": "https://apps-Nantes.beta.education.fr"
      },
      {
        "id": "nice",
        "name": "Académie de Nice",
        "url": "https://apps-Nice.beta.education.fr"
      },
      {
        "id": "normandie",
        "name": "Académie de Normandie",
        "url": "https://apps-Normandie.beta.education.fr"
      },
      {
        "id": "noumea",
        "name":"Académie de Nouméa",
        "url":"https://apps-outremer.beta.education.fr"
      },
      {
        "id": "orleans-tours",
        "name": "Académie d'Orléans-Tours",
        "url": "https://apps-Orleans-Tours.beta.education.fr"
      },
      {
        "id": "paris",
        "name": "Académie de Paris",
        "url": "https://apps-Paris.beta.education.fr"
      },
      {
        "id": "polynesie",
        "name":"Académie de Polynésie",
        "url":"https://apps-outremer.beta.education.fr"
      },
      {
        "id": "poitiers",
        "name": "Académie de Poitiers",
        "url": "https://apps-Poitiers.beta.education.fr"
      },
      {
        "id": "reims",
        "name": "Académie de Reims",
        "url": "https://apps-Reims.beta.education.fr"
      },
      {
        "id": "rennes",
        "name": "Académie de Rennes",
        "url": "https://apps-Rennes.beta.education.fr"
      },
      {
        "id": "reunion",
        "name":"Académie de La Réunion",
        "url":"https://apps-outremer.beta.education.fr"
      },
      {
        "id": "saint-pierre-et-miquelon",
        "name":"Académie de Saint-Pierre-et-Miquelon",
        "url":"https://apps-outremer.beta.education.fr"
      },
      {
        "id": "strasbourg",
        "name": "Académie de Strasbourg",
        "url": "https://apps-Strasbourg.beta.education.fr"
      },
      {
        "id": "toulouse",
        "name": "Académie de Toulouse",
        "url": "https://apps-Toulouse.beta.education.fr"
      },
      {
        "id": "versailles",
        "name": "Académie de Versailles",
        "url": "https://apps-Versailles.beta.education.fr"
      },
      {
        "id": "wallis-et-futuna",
        "name":"Académie de Wallis-et-Futuna",
        "url":"https://apps-outremer.beta.education.fr"
      },
      {
        "id": "administration-centrale",
        "name": "Administration centrale",
        "url": "https://apps-education.beta.education.fr"
      }
    ]
}