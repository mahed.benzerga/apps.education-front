export default [
   /* {
      "id": "webconference",
      "title": "Webconférence",
      "isDisabled": true,
      "subtitle": "Échanger en petit groupe",
      "icon": "webconference",
      "description": "Service de visio conférence via navigateur web.",
      "texts": [
         { "title": "Webconférence avec Jitsi Meet", 
           "body": `
                <p>Ce service de webconférence mis à disposition du Ministère de l’Education nationale et de la Jeunesse par Scaleway pendant l'état d'urgence sanitaire permet de maintenir la continuité des échanges entre les professionnels de l'éducation nationale. </p>
                <p>Il permet notamment&nbsp;:</p>
                <ul>
                  <li>de créer simplement une webconférence en invitant des participants éloignés par communication d'un lien&nbsp;;</li>
                  <li>de communiquer et d'échanger en partageant des écrans, des présentations, etc.&nbsp;; </li> 
                  <li>d'échanger par messagerie instantannée lors de la conférence.</li>
                </ul>
                <p>Ce service est compatible avec l'ensemble des navigateurs web récents quel que soit le terminal utilisé. Basé sur le logiciel libre Jitsi Meet, le service peut également être utilisé via une application mobile. Il favorise les interactions, les échanges et la mutualisation au sein d'un large public.</p>
           ` },
      ] 
   },*/
   {
      "id": "peertube",
      "title": "Peertube",
      "subtitle": "Vidéos",
      "description": "Publication et partage de vidéos",
      "icon": "peertube",
      "urls": {
        "corse": "https://tube-corse.beta.education.fr",
        "nice": "https://tube-nice.beta.education.fr",  
        "grenoble": "https://tube-grenoble.beta.education.fr",
        "administration-centrale": "https://tube-education.beta.education.fr/",
        "clermont-ferrand": "https://tube-clermont-ferrand.beta.education.fr",
        "bordeaux": "https://tube-bordeaux.beta.education.fr",
        "amiens": "https://tube-amiens.beta.education.fr",
        "nantes": "https://tube-nantes.beta.education.fr", 
        "poitiers": "https://tube-poitiers.beta.education.fr",  
        "montpellier": "https://tube-montpellier.beta.education.fr",  
        "limoges": "https://tube-limoges.beta.education.fr",  
        "lille": "https://tube-lille.beta.education.fr",  
        "orleans-tours": "https://tube-orleans-tours.beta.education.fr",  
        "dijon": "https://tube-dijon.beta.education.fr",  
        "besancon": "https://tube-besancon.beta.education.fr",  
        "aix-marseille": "https://tube-aix-marseille.beta.education.fr",  
        "normandie": "https://tube-normandie.beta.education.fr",  
        "lyon": "https://tube.ac-lyon.fr",
        "nancy": "https://tube-nancy.beta.education.fr",  
        "reims": "https://tube-reims.beta.education.fr", 
        "strasbourg": "https://tube-strasbourg.beta.education.fr",
        "guadeloupe": "https://tube-outremer.beta.education.fr",
        "guyane": "https://tube-outremer.beta.education.fr",
        "outre-mer": "https://tube-outremer.beta.education.fr",
        "mayotte": "https://tube-outremer.beta.education.fr",
        "martinique": "https://tube-outremer.beta.education.fr",
        "noumea": "https://tube-outremer.beta.education.fr",
        "polynesie": "https://tube-outremer.beta.education.fr",
        "reunion": "https://tube-outremer.beta.education.fr",
        "saint-pierre-et-miquelon": "https://tube-outremer.beta.education.fr",
        "wallis-et-futuna": "https://tube-outremer.beta.education.fr",
        "versailles": "https://tube-versailles.beta.education.fr", 
        "toulouse": "https://tube-toulouse.beta.education.fr", 
        "rennes": "https://tube-rennes.beta.education.fr",
        "paris": "https://tube-paris.beta.education.fr", 
        "creteil": "https://tube-creteil.beta.education.fr", 
      },
      "texts":[
          {"title":"Partagez vos vidéos avec Peertube","body":
          `<p>PeerTube est un service mis à disposition par le Ministère de l'Education Nationale et de la Jeunesse.</p>
          <p>Il permet de déposer, d'héberger et de consulter des média audios et vidéos. Il répond aux conditions préconisées dans le cadre du règlement général de la protection des données (RGPD).</p>
          <p>Ce service, à destination de l'ensemble des personnels de l'éducation nationale, permet&nbsp;:</p>
          <ul>
            <li>sans authentification de <strong>consulter</strong> les médias mis à disposition</li>
            <li>ou après authentification par l'adresse professionnelle, de <strong>déposer</strong>, <strong>partager</strong> ou <strong>télécharger des médias</strong>, dans le cadre de la charte d'utilisation. L'auteur reste responsable du contenu de ses productions et doit s’assurer qu’il dispose bien des droits de publication nécessaires.</li>
          </ul>`}
          ]
   },
   {
      "id": "etherpad",
      "title": "Etherpad",
      "subtitle": "Rédaction à plusieurs",
      "description": "Service de rédaction collaborative.",
      "icon": "etherpad",
      "urls": {
        "aix-marseille": "https://pad-Aix-Marseille.beta.education.fr",
        "amiens": "https://pad-Amiens.beta.education.fr",
        "besancon": "https://pad-Besancon.beta.education.fr",
        "bordeaux": "https://pad-Bordeaux.beta.education.fr",
        "clermont-ferrand": "https://pad-Clermont-Ferrand.beta.education.fr",
        "corse": "https://pad-Corse.beta.education.fr",
        "creteil": "https://pad-Creteil.beta.education.fr",
        "dijon": "https://pad-Dijon.beta.education.fr",
        "administration-centrale": "https://pad-education.beta.education.fr",
        "grenoble": "https://pad-Grenoble.beta.education.fr",
        "outre-mer": "https://pad-outremer.beta.education.fr",
        "lille": "https://pad-Lille.beta.education.fr",
        "limoges":"https://pad-Limoges.beta.education.fr",
        "lyon":"https://pad-Lyon.beta.education.fr",
        "montpellier": "https://pad-Montpellier.beta.education.fr",
        "nancy": "https://pad-Nancy.beta.education.fr",
        "nantes": "https://pad-Nantes.beta.education.fr",
        "nice":"https://pad-Nice.beta.education.fr",
        "normandie": "https://pad-Normandie.beta.education.fr",
        "orleans-tours":"https://pad-Orleans-Tours.beta.education.fr",
        "paris":"https://pad-Paris.beta.education.fr",
        "poitiers": "https://pad-Poitiers.beta.education.fr",
        "reims": "https://pad-Reims.beta.education.fr",
        "rennes": "https://pad-Rennes.beta.education.fr",
        "strasbourg": "https://pad-Strasbourg.beta.education.fr",
        "toulouse": "https://pad-Toulouse.beta.education.fr",
        "versailles": "https://pad-Versailles.beta.education.fr",
        "guadeloupe": "https://pad-outremer.beta.education.fr",
        "guyane": "https://pad-outremer.beta.education.fr",
        "martinique": "https://pad-outremer.beta.education.fr",
        "mayotte": "https://pad-outremer.beta.education.fr",
        "noumea": "https://pad-outremer.beta.education.fr",
        "polynesie": "https://pad-outremer.beta.education.fr",
        "reunion": "https://pad-outremer.beta.education.fr",
        "saint-pierre-et-miquelon": "https://pad-outremer.beta.education.fr",
        "wallis-et-futuna": "https://pad-outremer.beta.education.fr"
      },
      "texts":[
          {"title":"La rédaction collaborative avec Etherpad","body":
          `
          <p>Etherpad est un service en ligne de production de textes en mode collaboratif. Il permet à plusieurs utilisateurs distants d'écrire de manière synchrone ou asynchrone.<p>
          <p>Les contributions de chaque utilisateur qui sont signalées par un code couleur, apparaissent à l’écran et sont sauvegardées automatiquement en temps réel.</p>
          <p>D'autres utilisateurs peuvent être invités à collaborer sur le document par transmission du lien. Enfin, ce dernier peut être également téléchargé et exporté sous différents formats.</p>
          <p>Ce service peut être utilisé pour&nbsp;:</p>
          <ul>
            <li>éditer des comptes-rendus collectifs&nbsp;;</li> 
            <li>planifier des tâches au sein d'un groupe&nbsp;;</li>
            <li>rédiger des projets en mode collaboratif&nbsp;;</li>
            <li>documenter des résultats, des retours d'expérience&nbsp;.</li>
          </ul>
          <p>Dans le cadre des activités pédagogiques, les élèves vont pouvoir&nbsp;:</p>
          <ul>
            <li>développer des compétences sur le respect d'autrui et sur le travail de collaboration&nbsp;;</li>
            <li>échanger des points de vue différents sur un même sujet (prendre en compte et respecter le travail d'autrui, confronter ses idées en argumentant)&nbsp;;</li>
            <li>développer leur créativité, leur motivation et leur esprit critique&nbsp;;</li>
            <li>donner du sens à l’écriture (je n’écris pas que pour moi-même ou pour l’enseignant mais pour un groupe et éventuellement pour un public extérieur)&nbsp;.</li>
          </ul>
          <p>Pour l'enseignant, cet outil permet&nbsp;:</p>
          <ul>
            <li>d'observer l'implication de chacun&nbsp;;</li>
            <li>de guider les élèves et corriger les travaux en temps réel et au fur et à mesure de leur avancement&nbsp;;</li>
            <li>d'adopter une approche individualisée en dispensant des conseils ciblés sans altérer le sens du travail d’équipe&nbsp;;</li>
            <li>d'évaluer les productions au sein des groupes de travail&nbsp;.</li>
          </ul>
          `}
      ]
   },
   {
      "id": "nextcloud",
      "title": "Nextcloud",
      "subtitle": "Partage de documents",
      "description": "Service d'hébergement, gestion et partage de fichiers.",
      "icon": "nextcloud",
      "urls": {
        "corse": "https://nuage-corse.beta.education.fr",
        "nice": "https://nuage-nice.beta.education.fr",  
        "grenoble": "https://nuage-grenoble.beta.education.fr",
        "administration-centrale": "https://nuage-education.beta.education.fr",
        "clermont-ferrand": "https://nuage-clermont-ferrand.beta.education.fr",
        "bordeaux": "https://nuage-bordeaux.beta.education.fr",
        "amiens": "https://nuage-amiens.beta.education.fr",
        "nantes": "https://nuage-nantes.beta.education.fr", 
        "poitiers": "https://nuage-poitiers.beta.education.fr",  
        "montpellier": "https://nuage-montpellier.beta.education.fr",  
        "limoges": "https://nuage-limoges.beta.education.fr",  
        "lille": "https://nuage-lille.beta.education.fr",  
        "orleans-tours": "https://nuage-orleans-tours.beta.education.fr",  
        "dijon": "https://nuage-dijon.beta.education.fr",  
        "besancon": "https://nuage-besancon.beta.education.fr",  
        "aix-marseille": "https://nuage-aix-marseille.beta.education.fr",  
        "normandie": "https://nuage-normandie.beta.education.fr",  
        "lyon": "https://nuage-lyon.beta.education.fr",
        "nancy": "https://nuage-nancy.beta.education.fr",  
        "reims": "https://nuage-reims.beta.education.fr", 
        "strasbourg": "https://nuage-strasbourg.beta.education.fr",
        "guadeloupe": "https://nuage-outremer.beta.education.fr",
        "guyane": "https://nuage-outremer.beta.education.fr",
        "outre-mer": "https://nuage-outremer.beta.education.fr",
        "mayotte": "https://nuage-outremer.beta.education.fr",
        "martinique": "https://nuage-outremer.beta.education.fr",
        "noumea": "https://nuage-outremer.beta.education.fr",
        "polynesie": "https://nuage-outremer.beta.education.fr",
        "reunion": "https://nuage-outremer.beta.education.fr",
        "saint-pierre-et-miquelon": "https://nuage-outremer.beta.education.fr",
        "wallis-et-futuna": "https://nuage-outremer.beta.education.fr",
        "versailles": "https://monnuage.ac-versailles.fr", 
        "toulouse": "https://nuage-toulouse.beta.education.fr", 
        "rennes": "https://nuage-rennes.beta.education.fr",
        "paris": "https://nuage-paris.beta.education.fr", 
        "creteil": "https://nuage-creteil.beta.education.fr", 
      },
      "texts":[
          {"title":"Partager des fichiers avec Nextcloud","body":
          `
          <p>Nextcloud est un service <a href="https://primabord.eduscol.education.fr/qu-est-ce-que-l-informatique-dans-les-nuages-ou-le-cloud-computing" target="_blank">cloud</a> proposé par le Ministère de l'Education Nationale et de la Jeunesse.</p>
          <p>Il met à disposition un espace de stockage en ligne sécurisé pour diffuser et partager des documents et des ressources téléchargeables à partir d'un poste de travail ou d'un appareil mobile.</p>
          <p>Accessible après authentification par son adresse professionnelle, ce service facilite le travail collaboratif entre les personnels de l'Éducation nationale. Il répond aux conditions préconisées dans le cadre du règlement général de la protection des données (RGPD).</p>
          <p>Ce service permet&nbsp;:</p>
          <ul>
            <li>de stocker et de synchroniser les documents afin de les maintenir à jour en temps réel&nbsp;;</li>
            <li>d'enregistrer et d'accéder aux données à tout moment et en tout lieu&nbsp;;</li>
            <li>de travailler en toute sécurité en garantissant la confidentialité des données.&nbsp;;</li>
          </ul>
          <p>Chaque utilisateur disposera d'un espace de stockage d'une taille de 1 Go sauf réglage spécifique décidé localement.</p>
          `}
          ]
   },
   {
      "id": "blogs",
      "title": "Blogs",
      "subtitle": "Publication sur le web",
      "description": "Service d'édition et de publication de blogs.",
      "icon": "blogs",
      "urls": {
        "aix-marseille": "https://apps-Aix-Marseille.beta.education.fr/public",
        "amiens": "https://apps-Amiens.beta.education.fr/public",
        "besancon": "https://apps-Besancon.beta.education.fr/public",
        "bordeaux": "https://apps-Bordeaux.beta.education.fr/public",
        "clermont-ferrand": "https://apps-Clermont-Ferrand.beta.education.fr/public",
        "corse": "https://apps-Corse.beta.education.fr/public",
        "creteil": "https://apps-Creteil.beta.education.fr/public",
        "dijon": "https://apps-Dijon.beta.education.fr/public",
        "administration-centrale": "https://apps-education.beta.education.fr/public",
        "grenoble": "https://apps-Grenoble.beta.education.fr/public",
        "guadeloupe": "https://apps-outremer.beta.education.fr/public",
        "guyane": "https://apps-outremer.beta.education.fr/public",
        "outre-mer": "https://apps-outremer.beta.education.fr/public",
        "mayotte": "https://apps-outremer.beta.education.fr/public",
        "martinique": "https://apps-outremer.beta.education.fr/public",
        "noumea": "https://apps-outremer.beta.education.fr/public",
        "polynesie": "https://apps-outremer.beta.education.fr/public",
        "reunion": "https://apps-outremer.beta.education.fr/public",
        "saint-pierre-et-miquelon": "https://apps-outremer.beta.education.fr/public",
        "wallis-et-futuna": "https://apps-outremer.beta.education.fr/public",
        "lille": "https://apps-Lille.beta.education.fr/public",
        "limoges": "https://apps-Limoges.beta.education.fr/public",
        "lyon": "https://apps-Lyon.beta.education.fr/public",
        "montpellier": "https://apps-Montpellier.beta.education.fr/public",
        "nancy": "https://apps-Nancy.beta.education.fr/public",
        "nantes": "https://apps-Nantes.beta.education.fr/public",
        "nice": "https://apps-Nice.beta.education.fr/public",
        "normandie": "https://apps-Normandie.beta.education.fr/public",
        "orleans-tours": "https://apps-Orleans-Tours.beta.education.fr/public",
        "paris": "https://apps-Paris.beta.education.fr/public",
        "poitiers": "https://apps-Poitiers.beta.education.fr/public",
        "reims": "https://apps-Reims.beta.education.fr/public",
        "rennes": "https://apps-Rennes.beta.education.fr/public",
        "strasbourg": "https://apps-Strasbourg.beta.education.fr/public",
        "toulouse": "https://apps-Toulouse.beta.education.fr/public",
        "versailles": "https://apps-Versailles.beta.education.fr/public"
      },
      "texts":[
          {"title":"Publiez vos contenus avec le service de blogs","body":
          `
          <p>Un blog est un espace sur internet où l’on partage une expérience et où l'on donne un point de vue personnel (et professionnel dans notre contexte ) sur un sujet donné.</p>
          <p>Ce blog, à destination des personnels de l'Éducation nationale offre, après authentification, la possibilité de communiquer et de prolonger le travail collectif d'une classe ou d'un groupe de professionnels. Ce service sécurisé permet notamment de partager, selon une organisation définie par son auteur, des textes, des images, des vidéos ...</p>
          <p>Pour mémoire, l'utilisation d'un blog doit respecter une charte d'utilisation et des mentions légales.</p>
          <p>Ce service permet notamment&nbsp;:</p>
          <ul>
            <li>de former les élèves  à un usage éclairé et responsable du numérique&nbsp;;</li>
            <li>de sensibiliser les élèves à la protection des données personnelles, à la propriété intellectuelle et au droit à l’image&nbsp;;</li>
            <li>de développer l'esprit critique&nbsp;;</li>
            <li>de documenter les productions et les projets de classe.&nbsp;;</li>
          </ul>
          <p>Ce service est un outil de communication au sein de la communauté éducative pour&nbsp;:</p>
          <ul>
            <li>les élèves dans et en dehors du temps et de l'espace scolaires, il participe à la continuité pédagogique&nbsp;;</li>
            <li>les parents en lien avec la coéducation&nbsp;;</li>
            <li>les professionnels de l'éducation dans le cadre de leurs différentes missions.</li>
          </ul>
          `}
          ]
   },
   {
    "id": "codi-md",
    "title": "CodiMD",
    "subtitle": "Rédaction à plusieurs",
    "description": "Service de rédaction collaborative.",
    "icon": "etherpad",
    "texts":[
      {"title":"La rédaction collaborative avec Etherpad","body":
      `<p>CodiMD est un éditeur collaboratif de notes et diaporama, écrits en Markdown.<p><p>Il s'inspire d'Etherpad et d'autres éditeurs collaboratifs similaires..<p>`}
  ],
    "urls": {
      "aix-marseille": "https://codi-Aix-Marseille.beta.education.fr",
      "amiens": "https://codi-Amiens.beta.education.fr",
      "besancon": "https://codi-Besancon.beta.education.fr",
      "bordeaux": "https://codi-Bordeaux.beta.education.fr",
      "clermont-ferrand": "https://codi-Clermont-Ferrand.beta.education.fr",
      "corse": "https://codi-Corse.beta.education.fr",
      "creteil": "https://codi-Creteil.beta.education.fr",
      "dijon": "https://codi-Dijon.beta.education.fr",
      "administration-centrale": "https://codi-education.beta.education.fr",
      "grenoble": "https://codi-Grenoble.beta.education.fr",
      "outre-mer": "https://codi-outremer.beta.education.fr",
      "lille": "https://codi-Lille.beta.education.fr",
      "limoges":"https://codi-Limoges.beta.education.fr",
      "lyon":"https://codi-Lyon.beta.education.fr",
      "montpellier": "https://codi-Montpellier.beta.education.fr",
      "nancy": "https://codi-Nancy.beta.education.fr",
      "nantes": "https://codi-Nantes.beta.education.fr",
      "nice":"https://codi-Nice.beta.education.fr",
      "normandie": "https://codi-Normandie.beta.education.fr",
      "orleans-tours":"https://codi-Orleans-Tours.beta.education.fr",
      "paris":"https://codi-Paris.beta.education.fr",
      "poitiers": "https://codi-Poitiers.beta.education.fr",
      "reims": "https://codi-Reims.beta.education.fr",
      "rennes": "https://codi-Rennes.beta.education.fr",
      "strasbourg": "https://codi-Strasbourg.beta.education.fr",
      "toulouse": "https://codi-Toulouse.beta.education.fr",
      "versailles": "https://codi-Versailles.beta.education.fr",
      "guadeloupe": "https://codi-outremer.beta.education.fr",
      "guyane": "https://codi-outremer.beta.education.fr",
      "martinique": "https://codi-outremer.beta.education.fr",
      "mayotte": "https://codi-outremer.beta.education.fr",
      "noumea": "https://codi-outremer.beta.education.fr",
      "polynesie": "https://codi-outremer.beta.education.fr",
      "reunion": "https://codi-outremer.beta.education.fr",
      "saint-pierre-et-miquelon": "https://codi-outremer.beta.education.fr",
      "wallis-et-futuna": "https://codi-outremer.beta.education.fr"
    }
   },
   /*
   {
      "id": "forum",
      "title": "Discourse",
      "isDisabled": true,
      "subtitle": "Forums",
      "description": "Forum de discussion",
      "icon": "forum",
      "texts":[
          {"title":"Les forums de discussion Discourse","body":
          `
          <p>Discourse est un service de forums mis à disposition par le Ministère de l'Education Nationale et de la Jeunesse.</p>
          <p>Un forum est un dispositif interactif qui permet de mettre des contributions en ligne sur une page web et participer à des discussions sur un thème. Après authentification, les participants ont la possibilité d'échanger, de communiquer sur un sujet donné à l'aide d'une liste de diffusion et d'un modérateur qui régule les intéractions.</p>
          <p>La contribution peut porter sur un nouveau sujet (initialisant ainsi une nouvelle discussion) ou être une réponse, un commentaire à un autre message (au sein d'une même discussion).</p>
          <p>Discourse répond aux conditions préconisées dans le cadre du règlement général de la protection des données (RGPD).</p>
          <p>Il inclut des fonctionnalités comme&nbsp;:</p>
          <ul>
            <li>les mises à jour en direct&nbsp;;</li>
            <li>la prévisualisation de liens&nbsp;;</li>
            <li>le glisser-déposer des pièces jointes.</li>
          </ul>
          <p>D'un point de vue pédagogique, ce service permet notamment&nbsp;:<p>
          <ul>
            <li>de favoriser des échanges et des débats constructifs et structurés par le modérateur&nbsp;;</li>
            <li>de permettre à certains participants de s'exprimer davantage qu'en présentiel&nbsp;;</li>
            <li>de favoriser la réflexion par une production différée dans le temps&nbsp;;</li>
            <li>d'inciter chaque participant à publier pour enrichir la contribution collective sur le sujet proposé par l'animateur.</li> 
          </ul>
          <p>D'un point de vue relationnel, ce service permet&nbsp;:</p>
          <ul>
            <li>d'obtenir rapidement des réponses à ses questions&nbsp;;</li>
            <li>de faire appel aux participants plus experts ainsi qu'à l'animateur du forum&nbsp;;</li>
            <li>de renforcer les liens d'appartenance à un groupe, à une communauté notamment dans un contexte d'isolement&nbsp;;</li>
            <li>de contribuer à la persévérance scolaire.</li>
          </ul>
          `}
          ]
   } */
];